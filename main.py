import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()
import numpy
import tflearn
import tensorflow
import json
import pickle
import random
import os
import pyttsx3
from pygame import mixer
import speech_recognition as sr
from gtts import gTTS

engine = pyttsx3.init()
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[1].id)
volume = engine.getProperty('volume')
engine.setProperty('volume', 10.0)
rate = engine.getProperty('rate')
engine.setProperty('rate', rate - 25)

with open("intents.json") as file:
    data = json.load(file)

try:
    with open("data.pickle", "rb") as f:
        words, labels, training, output = pickle.load(f)
except:
    words = []
    labels = []
    docs_x = []
    docs_y = []

    for intent in data["intents"]:
        for pattern in intent["patterns"]:
            wrds = nltk.word_tokenize(pattern)
            words.extend(wrds)
            docs_x.append(wrds)
            docs_y.append(intent["tag"])

        if intent["tag"] not in labels:
            labels.append(intent["tag"])

    words = [stemmer.stem(w.lower()) for w in words if w != "?"]
    words = sorted(list(set(words)))

    labels = sorted(labels)

    training = []
    output = []

    out_empty = [0 for _ in range(len(labels))]

    for x, doc in enumerate(docs_x):
        bag = []

        wrds = [stemmer.stem(w.lower()) for w in doc]

        for w in words:
            if w in wrds:
                bag.append(1)
            else:
                bag.append(0)

        output_row = out_empty[:]
        output_row[labels.index(docs_y[x])] = 1

        training.append(bag)
        output.append(output_row)


    training = numpy.array(training)
    output = numpy.array(output)

    with open("data.pickle", "wb") as f:
        pickle.dump((words, labels, training, output), f)

tensorflow.reset_default_graph()
# this is the DNN structure

net = tflearn.input_data(shape=[None, len(training[0])]) # This is the input Layer
net = tflearn.fully_connected(net, 8) # this is the first hidden layer
net = tflearn.fully_connected(net, 8) # this is the second hidden layer
net = tflearn.fully_connected(net, len(output[0]), activation="softmax") # this is the Output layer
net = tflearn.regression(net)

model = tflearn.DNN(net)

try:
    with open('model.tflearn.index') as f:
        pass # Just pahila file cha ki chaina vanera hereko ho hai... it does nothing
    model.load("model.tflearn") # yesmaa tensorflow le session load garihalxa so tala problem aauxa...
    # tyo tutorial maa nai error sikako cha yo part ko.
except FileNotFoundError:
    model.fit(training, output, n_epoch=1000, batch_size=8, show_metric=True)
    model.save("model.tflearn")

def bag_of_words(s, words):
    bag = [0 for _ in range(len(words))]

    s_words = nltk.word_tokenize(s)
    s_words = [stemmer.stem(word.lower()) for word in s_words]

    for se in s_words:
        for i, w in enumerate(words):
            if w == se:
                bag[i] = 1
            
    return numpy.array(bag)


def chat():
    print("Start talking")
    while True:
        r = sr.Recognizer()
        def speak(audioString):
	        print(audioString)
	        tts = gTTS(text=audioString, lang='en')
	        tts.save("audio.mp3")
	        os.system("mpg321 audio.mp3")
        with sr.Microphone() as source:
            print("Tell me something:")
            audio = r.listen(source)
            try:
                print("You said:- " + r.recognize_google(audio))
            except sr.UnknownValueError:
                print("Could not understand audio")
                engine.say('I didnt get that. Rerun the code')
           

        results = model.predict([bag_of_words(r.recognize_google(audio), words)])
        results_index = numpy.argmax(results)
        tag = labels[results_index]

        for tg in data["intents"]:
            if tg['tag'] == tag:
                responses = tg['responses']

        answer = random.choice(responses)
        engine.say(answer)
        engine.runAndWait()

    


if __name__ == "__main__":
    chat()

